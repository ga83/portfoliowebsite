package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"
  "github.com/sendgrid/sendgrid-go"
	"github.com/sendgrid/sendgrid-go/helpers/mail"
)

// filled by config file
var CONFIG_PORT = -1
var CONFIG_EMAIL_SENDER = ""
var CONFIG_EMAIL_RECEIVER = ""
var CONFIG_EMAIL_PASSWORD = ""
var CONFIG_PUBLIC_HOST = ""
var CONFIG_CERT_LOCATION = ""
var CONFIG_KEY_LOCATION = ""
var CONFIG_USING_TLS = false
var CONFIG_SENDGRID_API_KEY = ""

type Configuration struct {
	Port           int
	PublicHostname string
	EmailSender    string
	EmailPassword  string
	CertLocation   string
	KeyLocation    string
	UsingTls       bool
	SendgridApiKey string
	EmailReceiver  string
}

type SignupResponse struct {
	Status  string // can be "ok", "bad_email", "unknown_error"
	Name    string // own name back to user
	Email   string // own email back to user
	Message string // own message back to user
}

func main() {

  // use ~/
	var filename = "/home/neon/conf/contact.conf"
	var configuration Configuration

	file, err := os.Open(filename)
	decoder := json.NewDecoder(file)
	err = decoder.Decode(&configuration)
	if err != nil {
		log.Println(err)
	}

	CONFIG_PORT = configuration.Port
	CONFIG_EMAIL_SENDER = configuration.EmailSender
	CONFIG_PUBLIC_HOST = configuration.PublicHostname
	CONFIG_EMAIL_PASSWORD = configuration.EmailPassword
	CONFIG_CERT_LOCATION = configuration.CertLocation
	CONFIG_KEY_LOCATION = configuration.KeyLocation
	CONFIG_USING_TLS = configuration.UsingTls
	CONFIG_SENDGRID_API_KEY = configuration.SendgridApiKey
  CONFIG_EMAIL_RECEIVER = configuration.EmailReceiver

	http.HandleFunc("/contact",
		func(w http.ResponseWriter, r *http.Request) {
			processContact(w, r)
		})


// test ****
  result := sendContactEmail("testprog", "testprog@testprog.com", "this is a test from testprog")
   log.Print(result)
// *********

      // status message
    log.Print("Listening on port " + fmt.Sprint(CONFIG_PORT))

    if CONFIG_USING_TLS == false {
        log.Print("not using tls")
        if err := http.ListenAndServe(":" + fmt.Sprint(CONFIG_PORT), nil); err != nil {
            log.Print(err)
        }
    } else {
        log.Print("using tls")
        if err := http.ListenAndServeTLS(":" + fmt.Sprint(CONFIG_PORT), CONFIG_CERT_LOCATION, CONFIG_KEY_LOCATION, nil); err != nil {
            log.Print(err)
        }
    }



}

func processContact(w http.ResponseWriter, r *http.Request) {

	log.Println("EP: processContact")

	// required before we can access form values
	r.ParseForm()

	var name = r.Form.Get("name")
	var email = r.Form.Get("email")
	var message = r.Form.Get("message")

//	log.Print("contact captured:" + name + ", " + email + ":\n" + message)
  log.Print("contact captured:")
  log.Print(name)
  log.Print(email)
  log.Print(message)

	var signupResponse SignupResponse
	var success string = ""

	signupResponse = SignupResponse{
		success,
		name,
		email,
		message}

	success = sendContactEmail(name, email, message)

	jsonData, err := json.Marshal(signupResponse)
	_ = err
	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Connection", "close")
	// need these (haven't checked individually) otherwise browser complains about preflight
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Methods", "POST")
	w.Header().Set("Access-Control-Allow-Headers", "*")
	w.Write(jsonData)

}

func sendContactEmail(name string, email string, message string) string {

	log.Println("EP: sendContactEmail")

  log.Println("sendgrid key: " + CONFIG_SENDGRID_API_KEY);

	from := mail.NewEmail("Portfolio Bot", CONFIG_EMAIL_SENDER)
	subject := "Portfolio - Contact"
	to := mail.NewEmail("Guy Aaltonen", CONFIG_EMAIL_RECEIVER)
	plainTextContent := "name: " + name + "\nemail: " + email + "\nmessage: " + message
	htmlContent := "name: " + name + "<br/>  email: " + email + "<br/>message: " + message
	emailMessage := mail.NewSingleEmail(from, subject, to, plainTextContent, htmlContent)
	client := sendgrid.NewSendClient(CONFIG_SENDGRID_API_KEY)
	response, err := client.Send(emailMessage)

	if err != nil {
		log.Println(err)
		return "unknown_error"
	} else {
		fmt.Println(response.StatusCode)
		fmt.Println(response.Body)
		fmt.Println(response.Headers)
		return "success"
	}

}
