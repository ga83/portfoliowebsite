import { Component, OnInit, Output } from '@angular/core';
import { Input } from '@angular/core';
import { Router } from '@angular/router';
import { EventEmitter } from '@angular/core';

@Component({
  selector: 'app-menu-bar',
  templateUrl: './menu-bar.component.html',
  styleUrls: ['./menu-bar.component.scss']
})
export class MenuBarComponent implements OnInit {

  @Input()
  public menuRows: Array<any>;

  @Output()
  public selected = new EventEmitter();

  public selectedString;

  public hoveredString;

  constructor(public router: Router) { }

  public setHovered(hovered: string) {
    this.hoveredString = hovered;
  }

  ngOnInit() {
    if (this.menuRows && this.menuRows.length > 0) {
      this.selectedString = this.menuRows[0].navTemplate;
    }
  }

  public onChange(row: any): void {
    this.selectedString = row.navTemplate;
    this.selected.emit(row.navTemplate);
  }
}
