import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { SidebarModule } from 'ng-sidebar';
import { GamesComponent } from './games/games.component';
import { LiveWallpapersComponent } from './live-wallpapers/live-wallpapers.component';
import { WorkComponent } from './work/work.component';
import { HomeComponent } from './home/home.component';
import { MenuBarComponent } from './menu-bar/menu-bar.component';
import { LinkComponent } from './link/link.component';
import { EmptyMenuBarComponent } from './empty-menu-bar/empty-menu-bar.component';
import { TechnologiesComponent } from './technologies/technologies.component';
import { ContactComponent } from './contact/contact.component';
import { ReactiveFormsModule } from '@angular/forms';
import { TechnologyCollageComponent } from './technology-collage/technology-collage.component';
import { LinkCollageComponent } from './link-collage/link-collage.component';

import { HttpClientModule } from '@angular/common/http';
import { ThreeDGraphicsComponent } from './three-dgraphics/three-dgraphics.component';
import { NgxYoutubePlayerModule } from 'ngx-youtube-player';
import { ImageThumbnailComponent } from './image-thumbnail/image-thumbnail.component';

import 'hammerjs';
import 'mousetrap';
import { GalleryModule } from '@ks89/angular-modal-gallery';


@NgModule({
  declarations: [
    AppComponent,
    GamesComponent,
    LiveWallpapersComponent,
    WorkComponent,
    HomeComponent,
    MenuBarComponent,
    LinkComponent,
    EmptyMenuBarComponent,
    TechnologiesComponent,
    ContactComponent,
    TechnologyCollageComponent,
    LinkCollageComponent,
    ThreeDGraphicsComponent,
    ImageThumbnailComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    SidebarModule.forRoot(),
    ReactiveFormsModule,
    HttpClientModule,
    NgxYoutubePlayerModule,
    GalleryModule
    
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
