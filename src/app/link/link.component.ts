import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-link',
  templateUrl: './link.component.html',
  styleUrls: ['./link.component.scss']
})
export class LinkComponent implements OnInit {

  @Input()
  appLink: string;

  @Input()
  sourceLink: string;

  @Input()
  linkTextInput: string;

  linkText: string;

  constructor() { }

  ngOnInit() {
    if(!this.linkTextInput) {
      this.linkText = 'Link';
    } else {
      this.linkText = this.linkTextInput;
    }
  }

}
