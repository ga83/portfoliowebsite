import { Component, OnInit } from '@angular/core';
import { FormGroup, ReactiveFormsModule, FormControl, ValidationErrors } from '@angular/forms';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Validators } from '@angular/forms';
import { ContactState } from './contact-status';


@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit {




  private IP: string;
  private PROTOCOL: string;

  private signupUrl: string;
  public contactForm: FormGroup;

  public contactState: ContactState;


  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/x-www-form-urlencoded' })
  };

  public email: string;
  public name: string;
  public message: string;
  public invalid: boolean = false;

  menuRows: Array<any>;
  template: string = '';


  // ****** LOCALHOST VS SERVER ***************
  private CONGIGURE_FOR_SERVER: boolean = true;
  // ******************************************


  constructor(
    private http: HttpClient
  ) {
    if (this.CONGIGURE_FOR_SERVER === true) {
      this.IP = 'guyaaltonen.dev';
      this.PROTOCOL = 'https://';
    } else {
      this.IP = 'localhost';
      this.PROTOCOL = 'http://';
    }

    this.signupUrl = this.PROTOCOL + this.IP + ':6969/contact';
  }

  ngOnInit() {
    this.initialiseFormGroup();
    this.contactState = ContactState.unsent;
    this.template = 'contact';
    this.initMenuRows();
  }

  public checkStatus(): void {
    if(
      this.contactForm.get('name').value == '' &&
      this.contactForm.get('email').value == '' &&
      this.contactForm.get('message').value == ''
      ) {

    } else {
      this.contactState = ContactState.unsent;
    }
  }

  private initMenuRows(): void {
    this.menuRows = [
      {
        navTitle: 'Contact Form',
        navTemplate: 'contact'
      }
      ];
  }

  private initialiseFormGroup(): void {

    this.contactForm = new FormGroup({
      name: new FormControl('', []),
      email: new FormControl('',
      [
        Validators.required,
        Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$")
      ]),
      message: new FormControl('', [])
    });
  }

  public ContactState(): any {
    return ContactState; 
  }

  public sendMessage(): void {
    console.log("sending message2");

   // console.log(JSON.stringify(this.contactForm));

    let contactData = { 
      'email': this.contactForm.get('email').value,
     'name': this.contactForm.get('name').value, 
     'message': this.contactForm.get('message').value 
    };

    console.log(JSON.stringify(contactData));

    this.http.post(this.signupUrl, this.getFormUrlEncoded(contactData), this.httpOptions).subscribe(
      (success) => {
        console.log("success: " + JSON.stringify(success));
        this.contactState = ContactState.success;
        this.contactForm.reset();
      },
      (error) => {
        console.log("error: " + JSON.stringify(error));
        this.contactState = ContactState.failure;
      }
    );
  }

  getFormUrlEncoded(toConvert) {
    const formBody = [];
    for (const property in toConvert) {
      const encodedKey = encodeURIComponent(property);
      const encodedValue = encodeURIComponent(toConvert[property]);
      formBody.push(encodedKey + '=' + encodedValue);
    }
    return formBody.join('&');
  }



}
