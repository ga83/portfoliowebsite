import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-work',
  templateUrl: './work.component.html',
  styleUrls: ['./work.component.scss']
})
export class WorkComponent implements OnInit {

  menuRows: Array<any>;
  template: string = '';

  constructor() { }

  ngOnInit() {
    this.template = 'inthecode';
    this.initMenuRows();
  }

  private initMenuRows(): void {
    this.menuRows = [
      {
        navTitle: 'In The Code',
        navTemplate: 'inthecode'
      },
      {
        navTitle: 'Efficiency Leaders',
        navTemplate: 'efficiencyleaders'
      }
    ];
  }

}
