import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { GamesComponent } from './games/games.component';
import { LiveWallpapersComponent } from './live-wallpapers/live-wallpapers.component';
import { WorkComponent } from './work/work.component';
import { HomeComponent } from './home/home.component';
import { TechnologiesComponent } from './technologies/technologies.component';
import { ContactComponent } from './contact/contact.component';
import { ThreeDGraphicsComponent } from './three-dgraphics/three-dgraphics.component';


const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'games', component: GamesComponent },
  { path: 'livewallpapers', component: LiveWallpapersComponent },
  { path: 'work', component: WorkComponent },
  { path: 'technologies', component: TechnologiesComponent },
  { path: 'contact', component: ContactComponent },
  { path: 'threedgraphics', component: ThreeDGraphicsComponent },
];


@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: false } )],
  exports: [RouterModule]
})
export class AppRoutingModule { }
