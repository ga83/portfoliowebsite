import { Component, OnInit } from '@angular/core';
import { LinkRow } from '../link-row';
import { TechnologyRow } from '../technology-row';
import { Subscription } from 'rxjs';
import { GridLayout, Image, LineLayout, ModalGalleryConfig, ModalGalleryRef, ModalGalleryService, ModalLibConfig, PlainGalleryComponent, PlainGalleryStrategy, PlainLibConfig } from '@ks89/angular-modal-gallery';
import * as libConfigs from './libconfigs';
import { DomSanitizer } from '@angular/platform-browser';



@Component({
  selector: 'app-three-dgraphics',
  templateUrl: './three-dgraphics.component.html',
  styleUrls: ['./three-dgraphics.component.scss']
})
export class ThreeDGraphicsComponent implements OnInit {

  modelTechnologyRows: TechnologyRow[];
  modelLinkRows: LinkRow[];
  modelLink: string = 'https://www.cgtrader.com/guardian2600';

  animationTechnologyRows: TechnologyRow[];
  animationLinkRows: LinkRow[];
  animationLink: string = 'https://www.youtube.com/channel/UCrL79ftzaGIDwgf6IZ7Y2pw';

  menuRows: Array<any>;
  template: string = '';

  public id: string[] = [ "TJZ9GhS4y4A", "j6-Lthdlbdk", "X1VWJ3fX2ik", "gF6lRuaFmZM", "GOK5fGA87_I", "r-gGkgW6QSI" ];


  player: any;
  playerVars: any;
  


  constructor(
    private modalGalleryService: ModalGalleryService, 
    private sanitizer: DomSanitizer 
  ) { 
    this.modelTechnologyRows = [TechnologyRow.blenderRow];
    this.modelLinkRows = [new LinkRow(LinkRow.cgtraderImage, this.modelLink)];

    this.animationTechnologyRows = [TechnologyRow.blenderRow];
    this.animationLinkRows = [new LinkRow(LinkRow.youtubeImage, this.animationLink)];

    
  }

  ngOnInit() {
    this.template = 'models';
    this.initMenuRows();
  }

  savePlayer(player) {
    this.player = player;
    console.log("player instance", player);
  }
  onStateChange(event) {
    console.log("player state", event.data);
  }

  private initMenuRows(): void {
    this.menuRows = [
      {
        navTitle: 'Models',
        navTemplate: 'models'
      },
      {
        navTitle: 'Animation',
        navTemplate: 'animation'
      },
    ];
  }

  public imagesModels: Image[] = [
    new Image(0, {
      img:
        './assets/model-guardian2600-render-01.png',
      extUrl: 'http://www.google.com'
    }),
    new Image(1, {
      img:
        './assets/model-guardian2600-render-02.png',
      extUrl: 'http://www.google.com'
    }),
    new Image(2, {
      img:
        './assets/model-guardian2600-render-03.png',
      extUrl: 'http://www.google.com'
    }),
    new Image(3, {
      img:
        './assets/model-jetpack-render-01.png',
      extUrl: 'http://www.google.com'
    }),
    new Image(4, {
      img:
        './assets/model-jetpack-render-02.png',
      extUrl: 'http://www.google.com'
    }),
    new Image(5, {
      img:
        './assets/model-jetpack-render-03.png',
      extUrl: 'http://www.google.com'
    }),
    new Image(6, {
      img:
        './assets/model-plasma-pistol-render-01.png',
      extUrl: 'http://www.google.com'
    }),
    new Image(7, {
      img:
        './assets/model-plasma-pistol-render-02.png',
      extUrl: 'http://www.google.com'
    }),
    new Image(8, {
      img:
        './assets/model-plasma-pistol-render-03.png',
      extUrl: 'http://www.google.com'
    }),
    new Image(9, {
      img:
        './assets/model-rocket-campus-01.png',
      extUrl: 'http://www.google.com'
    }),
    new Image(10, {
      img:
        './assets/model-bowl-many-stripes-03.png',
      extUrl: 'http://www.google.com'
    }),
    new Image(11, {
      img:
        './assets/model-bowl-many-stripes-05.png',
      extUrl: 'http://www.google.com'
    }),
    new Image(12, {
      img:
        './assets/model-freezer-02.png',
      extUrl: 'http://www.google.com'
    }),
    new Image(13, {
      img:
        './assets/model-freezer-03.png',
      extUrl: 'http://www.google.com'
    }),


  ];

  public CONFIG519: ModalLibConfig = libConfigs.LIBCONFIG_519;

  openModal(id: number, imagesArrayToUse: Image[], imageIndex: number, libConfig?: ModalLibConfig): void {
    if(imagesArrayToUse.length === 0) {
      console.error('Cannot open modal-gallery because images array cannot be empty');
      return;
    }
    if(imageIndex > imagesArrayToUse.length - 1) {
      console.error('Cannot open modal-gallery because imageIndex must be valid');
      return;
    }
    const imageToShow: Image = imagesArrayToUse[imageIndex];
    const dialogRef: ModalGalleryRef = this.modalGalleryService.open({
      id,
      images: imagesArrayToUse,
      currentImage: imageToShow,
      libConfig,
    } as ModalGalleryConfig) as ModalGalleryRef;
  }
  
    // subscriptions to receive events from the gallery
  // REMEMBER TO call unsubscribe(); in ngOnDestroy (see below)
  private closeSubscription: Subscription | undefined;
  private showSubscription: Subscription | undefined;
  private firstImageSubscription: Subscription | undefined;
  private lastImageSubscription: Subscription | undefined;
  private hasDataSubscription: Subscription | undefined;
  private buttonBeforeHookSubscription: Subscription | undefined;
  private buttonAfterHookSubscription: Subscription | undefined;

  ngOnDestroy(): void {
    // release resources to prevent memory leaks and unexpected behaviours
    if (this.closeSubscription) {
      this.closeSubscription.unsubscribe();
    }
    if (this.showSubscription) {
      this.showSubscription.unsubscribe();
    }
    if (this.firstImageSubscription) {
      this.firstImageSubscription.unsubscribe();
    }
    if (this.lastImageSubscription) {
      this.lastImageSubscription.unsubscribe();
    }
    if (this.hasDataSubscription) {
      this.hasDataSubscription.unsubscribe();
    }
    if (this.buttonBeforeHookSubscription) {
      this.buttonBeforeHookSubscription.unsubscribe();
    }
    if (this.buttonAfterHookSubscription) {
      this.buttonAfterHookSubscription.unsubscribe();
    }
  }

}
