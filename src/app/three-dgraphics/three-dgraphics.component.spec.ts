import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ThreeDGraphicsComponent } from './three-dgraphics.component';

describe('ThreeDGraphicsComponent', () => {
  let component: ThreeDGraphicsComponent;
  let fixture: ComponentFixture<ThreeDGraphicsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ThreeDGraphicsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ThreeDGraphicsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
