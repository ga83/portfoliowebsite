import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'PortfolioWebsite';

  public selectedString: string;

  public hoveredString;

  constructor(private router: Router) {
    //
  }

  public setHovered(hovered: string) {
    this.hoveredString = hovered;
  }

  ngOnInit() {
    this.selectedString = 'home';
    this.router.navigate(['']);
  }

  public openHome(): void {
    this.selectedString = 'home';
    this.router.navigate(['']);
  }

  public openGames(): void {
    this.selectedString = 'games';
    this.router.navigate(['games']);
  }

  public openLiveWallpapers(): void {
    this.selectedString = 'livewallpapers';
    this.router.navigate(['livewallpapers']);
  }

  public open3dGraphics(): void {
    this.selectedString = 'threedgraphics';
    this.router.navigate(['threedgraphics']);
  }

    public openWork(): void {
    this.selectedString = 'work';
    this.router.navigate(['work']);
  }

  public openTechnologies(): void {
    this.selectedString = 'technologies';
    this.router.navigate(['technologies']);
  }

  public openContact(): void {
    this.selectedString = 'contact';
    this.router.navigate(['contact']);
  }


}
