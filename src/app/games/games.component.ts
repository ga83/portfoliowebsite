import { Component, OnInit } from '@angular/core';
import { TechnologyRow } from '../technology-row';
import { LinkRow } from '../link-row';


@Component({
  selector: 'app-games',
  templateUrl: './games.component.html',
  styleUrls: ['./games.component.scss']
})
export class GamesComponent implements OnInit {

  triviaTechnologyRows: TechnologyRow[];
  triviaLinkRows: LinkRow[];
  triviaLink: string = 'https://dolphintrivia.com';

  highwayTechnologyRows: TechnologyRow[];
  highwayLinkRows: LinkRow[];
  highwayLink: string = 'https://play.google.com/store/apps/details?id=com.beachcafesoftware.retroracerandroid2'

  kirraTechnologyRows: TechnologyRow[];
  kirraLinkRows: LinkRow[];
  kirraLink: string = 'https://play.google.com/store/apps/details?id=com.beachcafesoftware.retroracerandroid2'

  menuRows: Array<any>;
  template: string = '';

  public player: any;
  public playerVars: any;


  public racingid: string[] = [
    'agtvkHUo4rA', 
];

public kirraid: string[] = [
  'FtAYIJ8TC3U', 
];


  constructor() { 
    this.triviaTechnologyRows = [TechnologyRow.typescriptRow, TechnologyRow.angularRow, TechnologyRow.golangRow, TechnologyRow.mysqlRow];
    this.triviaLinkRows = [new LinkRow(LinkRow.dolphinImage, this.triviaLink)];

    this.highwayTechnologyRows = [TechnologyRow.javaRow, TechnologyRow.libgdxRow];
    //this.highwayLinkRows = [new LinkRow(LinkRow.playImage, this.highwayLink)];
    this.highwayLinkRows = [];

    this.kirraTechnologyRows = [TechnologyRow.javaRow, TechnologyRow.libgdxRow, TechnologyRow.openGlRow];
    //this.kirraLinkRows = [new LinkRow(LinkRow.dolphinImage, this.triviaLink)];
    this.kirraLinkRows = [];
  }

  onStateChange(event) {
    console.log("player state", event.data);
  }


  ngOnInit() {
    this.template = 'dolphintrivia';
    //this.template = 'highwayrace';
    this.initMenuRows();
  }

  savePlayer(player) {
    this.player = player;
    console.log("player instance", player);
  }

  private initMenuRows(): void {
    this.menuRows = [
      
      {
        navTitle: 'Dolphin Trivia',
        navTemplate: 'dolphintrivia'
      },
      /*
      {
        navTitle: 'Golden Eagle Racing',
        navTemplate: 'highwayrace'
      },
    */
      {
        navTitle: 'Kirra',
        navTemplate: 'kirra'
      }

    ];
  }


}
