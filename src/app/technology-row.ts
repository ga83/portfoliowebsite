export class TechnologyRow {

    // the images and links are always the same, because they are just just the technology's homepage.
    static angularRow: TechnologyRow = { image: 'logo-angular.png', link: 'https://angular.io/' };
    static libgdxRow: TechnologyRow = { image: 'logo-libgdx.png', link: 'https://libgdx.com/' };
    static blenderRow: TechnologyRow = { image: 'logo-blender.png', link: 'https://www.blender.org/' };
    static mysqlRow: TechnologyRow = { image: 'logo-mysql.png', link: 'https://www.mysql.com/' };
    static golangRow: TechnologyRow = { image: 'logo-golang.png', link: 'https://go.dev/' };
    static javaRow: TechnologyRow = { image: 'logo-java.png', link: 'https://www.java.com/' };
    static openGlRow: TechnologyRow = { image: 'logo-opengl.jpg', link: 'https://www.opengl.org/' };
    static phpRow: TechnologyRow = { image: 'logo-php.png', link: 'https://www.php.net/' };
    static typescriptRow: TechnologyRow = { image: 'logo-typescript.png', link: 'https://www.typescriptlang.org/' };
   
    public image: string;
    public link: string;
}
