export class LinkRow {

    // only the images are reusable in this class, because each link is different.
    static gitlabImage: string = 'logo-gitlab.png';
    static playImage: string = 'logo-play.png';
    static dolphinImage: string = 'logo-dolphintrivia.png';
    static cgtraderImage: string = 'logo-cgtrader.png';
    static youtubeImage: string = 'logo-youtube.png';

    public image: string;
    public link: string;

    constructor(image: string, link: string) {
        this.image = image;
        this.link = link;
    }
}
