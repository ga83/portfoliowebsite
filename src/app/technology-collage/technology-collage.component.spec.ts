import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TechnologyCollageComponent } from './technology-collage.component';

describe('TechnologyCollageComponent', () => {
  let component: TechnologyCollageComponent;
  let fixture: ComponentFixture<TechnologyCollageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TechnologyCollageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TechnologyCollageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
