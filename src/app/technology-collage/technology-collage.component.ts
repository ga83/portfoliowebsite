import { Component, Input, OnInit } from '@angular/core';
import { TechnologyRow } from '../technology-row';


@Component({
  selector: 'app-technology-collage',
  templateUrl: './technology-collage.component.html',
  styleUrls: ['./technology-collage.component.scss']
})
export class TechnologyCollageComponent implements OnInit {

  @Input()
  technologyRows: TechnologyRow[];

  constructor() { }

  ngOnInit() {
  }

}
