import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LinkCollageComponent } from './link-collage.component';

describe('LinkCollageComponent', () => {
  let component: LinkCollageComponent;
  let fixture: ComponentFixture<LinkCollageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LinkCollageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LinkCollageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
