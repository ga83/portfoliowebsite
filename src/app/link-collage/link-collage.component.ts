import { Component, Input, OnInit } from '@angular/core';
import { LinkRow } from '../link-row';


@Component({
  selector: 'app-link-collage',
  templateUrl: './link-collage.component.html',
  styleUrls: ['./link-collage.component.scss']
})
export class LinkCollageComponent implements OnInit {

  @Input()
  linkRows: LinkRow[];

  constructor() { }

  ngOnInit() {
  }

}
