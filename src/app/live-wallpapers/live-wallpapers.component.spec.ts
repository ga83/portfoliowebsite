import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { YoutubePlayerModule } from "ngx-youtube-player";

import { LiveWallpapersComponent } from './live-wallpapers.component';

describe('LiveWallpapersComponent', () => {
  let component: LiveWallpapersComponent;
  let fixture: ComponentFixture<LiveWallpapersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LiveWallpapersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LiveWallpapersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
