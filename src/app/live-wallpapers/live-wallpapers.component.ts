import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { LinkRow } from '../link-row';
import { TechnologyRow } from '../technology-row';
import { MenuBarComponent } from '../menu-bar/menu-bar.component';
import { GridLayout, Image, LineLayout, ModalGalleryConfig, ModalGalleryRef, ModalGalleryService, ModalLibConfig, PlainGalleryComponent, PlainGalleryStrategy, PlainLibConfig } from '@ks89/angular-modal-gallery';
import { DomSanitizer } from '@angular/platform-browser';
import * as libConfigs from './libconfigs';
import { Subscription } from 'rxjs';


@Component({
  selector: 'app-live-wallpapers',
  templateUrl: './live-wallpapers.component.html',
  styleUrls: ['./live-wallpapers.component.scss']
})
export class LiveWallpapersComponent implements OnInit {

  

  kenshoTechnologyRows: TechnologyRow[];
  kenshoLinkRows: LinkRow[];
  kenshoPlayLink: string = 'https://play.google.com/store/apps/details?id=com.beachcafesoftware.kensholite';
  kenshoGitlabLink: string = 'https://gitlab.com/ga83/kensholite';

  bokehTechnologyRows: TechnologyRow[];
  bokehLinkRows: LinkRow[];
  bokehPlayLink: string = 'https://play.google.com/store/apps/details?id=com.beachcafesoftware.bokeh';
  bokehGitlabLink: string = 'https://gitlab.com/ga83/bokehwallpaper';

  ribbonsTechnologyRows: TechnologyRow[];
  ribbonsLinkRows: LinkRow[];
  ribbonsPlayLink: string = 'https://play.google.com/store/apps/details?id=com.beachcafesoftware.ribbons';
  ribbonsGitlabLink: string = 'https://gitlab.com/ga83/ribbons';

  collageTechnologyRows: TechnologyRow[];
  collageLinkRows: LinkRow[];
  collagePlayLink: string = 'https://play.google.com/store/apps/details?id=com.tuxware.collage';
  collageGitlabLink: string = 'https://gitlab.com/ga83/collagewallpaper';

  tilesTechnologyRows: TechnologyRow[];
  tilesLinkRows: LinkRow[];
  tilesPlayLink: string = 'https://play.google.com/store/apps/details?id=com.tuxware.mesh';
  tilesGitlabLink: string = 'https://gitlab.com/ga83/tiles';

  nasaTechnologyRows: TechnologyRow[];
  nasaLinkRows: LinkRow[];
  nasaPlayLink: string = 'https://play.google.com/store/apps/details?id=com.tux.nasaapodmuzei';
  nasaGitlabLink: string = 'https://gitlab.com/ga83/nasaapodmuzei';

  citiesTechnologyRows: TechnologyRow[];
  citiesLinkRows: LinkRow[];
  citiesPlayLink: string = 'https://play.google.com/store/apps/details?id=tux.com.citiesmuzei';
  citiesGitlabLink: string = 'https://gitlab.com/ga83/citiesmuzei';

  menuRows: Array<any>;
  template: string = '';

  constructor(
    private modalGalleryService: ModalGalleryService, 
    private sanitizer: DomSanitizer   
    )
     { 
    this.kenshoTechnologyRows = [TechnologyRow.javaRow, TechnologyRow.libgdxRow, TechnologyRow.openGlRow];
    this.kenshoLinkRows = [new LinkRow(LinkRow.playImage, this.kenshoPlayLink), new LinkRow(LinkRow.gitlabImage, this.kenshoGitlabLink)];

    this.bokehTechnologyRows = [TechnologyRow.javaRow, TechnologyRow.libgdxRow, TechnologyRow.openGlRow];
    this.bokehLinkRows = [new LinkRow(LinkRow.playImage, this.bokehPlayLink), new LinkRow(LinkRow.gitlabImage, this.bokehGitlabLink)];

    this.ribbonsTechnologyRows = [TechnologyRow.javaRow, TechnologyRow.libgdxRow, TechnologyRow.openGlRow];
    this.ribbonsLinkRows = [new LinkRow(LinkRow.playImage, this.ribbonsPlayLink), new LinkRow(LinkRow.gitlabImage, this.ribbonsGitlabLink)];

    this.collageTechnologyRows = [TechnologyRow.javaRow, TechnologyRow.libgdxRow, TechnologyRow.openGlRow];
    this.collageLinkRows = [new LinkRow(LinkRow.playImage, this.collagePlayLink), new LinkRow(LinkRow.gitlabImage, this.collageGitlabLink)];

    this.tilesTechnologyRows = [TechnologyRow.javaRow, TechnologyRow.libgdxRow, TechnologyRow.openGlRow];
    this.tilesLinkRows = [new LinkRow(LinkRow.playImage, this.tilesPlayLink), new LinkRow(LinkRow.gitlabImage, this.tilesGitlabLink)];

    this.nasaTechnologyRows = [TechnologyRow.javaRow];
    this.nasaLinkRows = [new LinkRow(LinkRow.playImage, this.nasaPlayLink), new LinkRow(LinkRow.gitlabImage, this.nasaGitlabLink)];

    this.citiesTechnologyRows = [TechnologyRow.javaRow, TechnologyRow.phpRow, TechnologyRow.mysqlRow];
    this.citiesLinkRows = [new LinkRow(LinkRow.playImage, this.citiesPlayLink), new LinkRow(LinkRow.gitlabImage, this.citiesGitlabLink)];
  }

  ngOnInit() {
    this.template = 'kensho';
    this.initMenuRows();
  }


  @ViewChild('previewsTemplate')
  previewsTemplate?: TemplateRef<HTMLElement>;


  private count = 0;
  imageIndex = 0;
  galleryId = 1;
  isPlaying = true;

  public player: any;
  public playerVars: any;



  public ribbonsid: string[] = [
    'VGurAeKZSng', 
    'gzaMzS7P5b0', 
  'Sqc70DoygmE'
];

public bokehid: string[] = [
  't9hwvy8ki1g'
];

public kenshoid: string[] = [
  'jACci4jGH2M',
  'wbSX8YVtlW8'
];

public collageid: string[] = [
  'BqH7UK2RXqc', 
  '0dl6gYeELTI', 
'8CbS41xcfEw'
];

public tilesid: string[] = [
  'dUcO3uJlcEU', 
  'p25-IsmiPz0', 
'to-j0kEbFtE',
'BR3OtfNzVmI',
'vasBvciHNxw',
'W0N54FkvpX4'
];


public imagesNasa: Image[] = [
  new Image(0, {
    img:
      './assets/image-nasa-01.png',
    extUrl: 'http://www.google.com'
  }),
  new Image(1, {
    img:
      './assets/image-nasa-02.png',
    extUrl: 'http://www.google.com'
  }),
  new Image(2, {
    img:
      './assets/image-nasa-03.png',
    extUrl: 'http://www.google.com'
  }),
  new Image(3, {
    img:
      './assets/image-nasa-04.png',
    extUrl: 'http://www.google.com'
  }),
  new Image(4, {
    img:
      './assets/image-nasa-05.png',
    extUrl: 'http://www.google.com'
  }),
];

public imagesWallpaper: Image[] = [
    new Image(0, {
      img:
        './assets/image-cities-01.png',
      extUrl: 'http://www.google.com'
    }),
    new Image(1, {
      img:
        './assets/image-cities-02.png',
      extUrl: 'http://www.google.com'
    }),
    new Image(2, {
      img:
        './assets/image-cities-03.png',
      extUrl: 'http://www.google.com'
    }),
    new Image(3, {
      img:
        './assets/image-cities-04.png',
      extUrl: 'http://www.google.com'
    }),
    new Image(4, {
      img:
        './assets/image-cities-05.png',
      extUrl: 'http://www.google.com'
    }),
  ];

  // array with a single image inside (the first one)
  singleImage: Image[] = [this.imagesWallpaper[0]];


public CONFIG519: ModalLibConfig = libConfigs.LIBCONFIG_519;


openModal(id: number, imagesArrayToUse: Image[], imageIndex: number, libConfig?: ModalLibConfig): void {
  if(imagesArrayToUse.length === 0) {
    console.error('Cannot open modal-gallery because images array cannot be empty');
    return;
  }
  if(imageIndex > imagesArrayToUse.length - 1) {
    console.error('Cannot open modal-gallery because imageIndex must be valid');
    return;
  }
  const imageToShow: Image = imagesArrayToUse[imageIndex];
  const dialogRef: ModalGalleryRef = this.modalGalleryService.open({
    id,
    images: imagesArrayToUse,
    currentImage: imageToShow,
    libConfig,
  } as ModalGalleryConfig) as ModalGalleryRef;
}

savePlayer(player) {
    this.player = player;
    console.log("player instance", player);
  }

  onStateChange(event) {
    console.log("player state", event.data);
  }

  private initMenuRows(): void {
    this.menuRows = [
      {
        navTitle: 'Kensho',
        navTemplate: 'kensho'
      },
      {
        navTitle: 'Bokeh',
        navTemplate: 'bokeh'
      },
      {
        navTitle: 'Ribbons',
        navTemplate: 'ribbons'
      },
      {
        navTitle: 'Collage',
        navTemplate: 'collage'
      },
      {
        navTitle: 'Tiles',
        navTemplate: 'tiles'
      },
      {
        navTitle: 'NASA',
        navTemplate: 'muzei_nasa'
      },
      {
        navTitle: 'Cities',
        navTemplate: 'muzei_cities'
      },
    ];
  }

  // subscriptions to receive events from the gallery
  // REMEMBER TO call unsubscribe(); in ngOnDestroy (see below)
  private closeSubscription: Subscription | undefined;
  private showSubscription: Subscription | undefined;
  private firstImageSubscription: Subscription | undefined;
  private lastImageSubscription: Subscription | undefined;
  private hasDataSubscription: Subscription | undefined;
  private buttonBeforeHookSubscription: Subscription | undefined;
  private buttonAfterHookSubscription: Subscription | undefined;

  ngOnDestroy(): void {
    // release resources to prevent memory leaks and unexpected behaviours
    if (this.closeSubscription) {
      this.closeSubscription.unsubscribe();
    }
    if (this.showSubscription) {
      this.showSubscription.unsubscribe();
    }
    if (this.firstImageSubscription) {
      this.firstImageSubscription.unsubscribe();
    }
    if (this.lastImageSubscription) {
      this.lastImageSubscription.unsubscribe();
    }
    if (this.hasDataSubscription) {
      this.hasDataSubscription.unsubscribe();
    }
    if (this.buttonBeforeHookSubscription) {
      this.buttonBeforeHookSubscription.unsubscribe();
    }
    if (this.buttonAfterHookSubscription) {
      this.buttonAfterHookSubscription.unsubscribe();
    }
  }

}
