import { Component, OnInit } from '@angular/core';
import { TechnologyRow } from '../technology-row';
import { LinkRow } from '../link-row';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  technologyRows: TechnologyRow[];
  linkRows: LinkRow[];
  portfolioLink: string = "https://gitlab.com/ga83/portfoliowebsite";

  menuRows: Array<any>;
  template: string = '';

  constructor() {
    this.technologyRows = [TechnologyRow.typescriptRow, TechnologyRow.angularRow, TechnologyRow.golangRow];
    this.linkRows = [new LinkRow(LinkRow.gitlabImage, this.portfolioLink)];

   }


  ngOnInit() {
    this.template = 'home';
    this.initMenuRows();    
  }

  private initMenuRows(): void {
    this.menuRows = [
      {
        navTitle: 'Home Page',
        navTemplate: 'home'
      }
      ];
  }

}
