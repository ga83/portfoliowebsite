import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-image-thumbnail',
  templateUrl: './image-thumbnail.component.html',
  styleUrls: ['./image-thumbnail.component.scss']
})
export class ImageThumbnailComponent implements OnInit {

  @Input()
  image: string;

  @Input()
  height: number;

  @Input()
  link: string;

  constructor() { }

  ngOnInit() {

  }

}
