import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmptyMenuBarComponent } from './empty-menu-bar.component';

describe('EmptyMenuBarComponent', () => {
  let component: EmptyMenuBarComponent;
  let fixture: ComponentFixture<EmptyMenuBarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmptyMenuBarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmptyMenuBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
