import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-empty-menu-bar',
  templateUrl: './empty-menu-bar.component.html',
  styleUrls: ['./empty-menu-bar.component.scss']
})
export class EmptyMenuBarComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
