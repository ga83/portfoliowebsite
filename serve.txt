can use:
nvm use 16.17.0
ng serve

or:
export NODE_OPTIONS=--openssl-legacy-provider
nvm use 18.8.0
ng serve
